/* Pascal's Triangle lists the coefficients of the expanded polynomial
 * (x + y) ^ n, where n is the index of the row.  The binomial coefficients
 * can be calculated as a ratio of factorials, n!/(k! * (n-k)!). This is
 * equivalent to (n*(n-1)!) / (k * (k-1)! * (n-k)!), which means that the
 * coefficients can be determined recursively by equating binom(n, k)
 * with binom(n-1, k-1) * n/k.  Note that the recursion stops when k = 0,
 * in which case the coefficient is 1.
 *
 * This program prints a row of Pascal's Triangle by recursively calculating
 * binom(n, k) for k in [0,n].
 */

#include <stdio.h>
#include <stdlib.h>

// declare binom function
unsigned long binom(int n, int k);

int main()
{
    int n;

    // get the row index from the user
    printf("Enter a row index: ");
    scanf("%d", &n);

    // print each coefficient in the row
    int k;
    for (k = 0; k <= n; k ++) {
        // print a space before each coefficient except the first one
        if (k != 0) {
            printf(" ");
        }
        // calculate using recursive formula
        unsigned long coeff = binom(n, k);
        printf("%lu", coeff);
    }
    printf("\n");

    return 0;
}

// recursive function to calculate binomial coefficient given n, k
unsigned long binom(int n, int k) {
    // recursion base case
    if (k == 0) {
        return 1;
    }
    // each row is symmetric about the center,
    // so we can just recalculate the left side
    if (k > n/2) {
        return binom(n, n-k);
    }
    // binom(n,k) == binom(n-1, k-1) * n/k
    else {
        return binom(n-1, k-1) * n / k;
    }
}
