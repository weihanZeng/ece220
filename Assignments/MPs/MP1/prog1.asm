; The following program prints a histogram containing the frequency of
; each letter in a specified string.  All non-alphabetic characters are
; counted together and there is no distinction between upper- and lower-case
; letters.  The string to be analyzed must be stored at memory location x4000.
; After calculating the histogram, the program then prints the count of each
; character, one per line, in hexadecimal format.


;
; The code given to you here implements the histogram calculation that
; we developed in class.  In programming studio, we will add code that
; prints a number in hexadecimal to the monitor.
;
; Your assignment for this program is to combine these two pieces of
; code to print the histogram to the monitor.
;
; If you finish your program,
;    ** commit a working version to your repository  **
;    ** (and make a note of the repository version)! **


    .ORIG    x3000        ; starting address is x3000


;
; Count the occurrences of each letter (A to Z) in an ASCII string
; terminated by a NUL character.  Lower case and upper case should
; be counted together, and a count also kept of all non-alphabetic
; characters (not counting the terminal NUL).
;
; The string starts at x4000.
;
; The resulting histogram (which will NOT be initialized in advance)
; should be stored starting at x3F00, with the non-alphabetic count
; at x3F00, and the count for each letter in x3F01 (A) through x3F1A (Z).
;
; table of register use in this part of the code
;    R0 holds a pointer to the histogram (x3F00)
;    R1 holds a pointer to the current position in the string
;       and as the loop count during histogram initialization
;    R2 holds the current character being counted
;       and is also used to point to the histogram entry
;    R3 holds the additive inverse of ASCII '@' (xFFC0)
;    R4 holds the difference between ASCII '@' and 'Z' (xFFE6)
;    R5 holds the difference between ASCII '@' and '`' (xFFE0)
;    R6 is used as a temporary register
;

    LD R0,HIST_ADDR     ; point R0 to the start of the histogram

    ; fill the histogram with zeroes
    AND R6,R6,#0        ; put a zero into R6
    LD R1,NUM_BINS      ; initialize loop count to 27
    ADD R2,R0,#0        ; copy start of histogram into R2

    ; loop to fill histogram starts here
HFLOOP
    STR R6,R2,#0        ; write a zero into histogram
    ADD R2,R2,#1        ; point to next histogram entry
    ADD R1,R1,#-1       ; decrement loop count
    BRp HFLOOP          ; continue until loop count reaches zero

    ; initialize R1, R3, R4, and R5 from memory
    LD R3,NEG_AT        ; set R3 to additive inverse of ASCII '@'
    LD R4,AT_MIN_Z      ; set R4 to difference between ASCII '@' and 'Z'
    LD R5,AT_MIN_BQ     ; set R5 to difference between ASCII '@' and '`'
    LD R1,STR_START     ; point R1 to start of string

    ; the counting loop starts here
COUNTLOOP
    LDR R2,R1,#0        ; read the next character from the string
    BRz PRINT_HIST      ; found the end of the string

    ADD R2,R2,R3        ; subtract '@' from the character
    BRp AT_LEAST_A      ; branch if > '@', i.e., >= 'A'
NON_ALPHA
    LDR R6,R0,#0        ; load the non-alpha count
    ADD R6,R6,#1        ; add one to it
    STR R6,R0,#0        ; store the new non-alpha count
    BRnzp GET_NEXT      ; branch to end of conditional structure
AT_LEAST_A
    ADD R6,R2,R4        ; compare with 'Z'
    BRp MORE_THAN_Z     ; branch if > 'Z'

; note that we no longer need the current character
; so we can reuse R2 for the pointer to the correct
; histogram entry for incrementing
ALPHA
    ADD R2,R2,R0        ; point to correct histogram entry
    LDR R6,R2,#0        ; load the count
    ADD R6,R6,#1        ; add one to it
    STR R6,R2,#0        ; store the new count
    BRnzp GET_NEXT      ; branch to end of conditional structure

; subtracting as below yields the original character minus '`'
MORE_THAN_Z
    ADD R2,R2,R5        ; subtract '`' - '@' from the character
    BRnz NON_ALPHA      ; if <= '`', i.e., < 'a', go increment non-alpha
    ADD R6,R2,R4        ; compare with 'z'
    BRnz ALPHA          ; if <= 'z', go increment alpha count
    BRnzp NON_ALPHA     ; otherwise, go increment non-alpha

GET_NEXT
    ADD R1,R1,#1        ; point to next character in string
    BRnzp COUNTLOOP     ; go to start of counting loop



; The next bit of code prints the histogram calculated above with each
; bin of the histogram on a separate line.  All non-alpha characters are
; counted together and the count is displayed in hexadecimal format on the
; '@' row.  Each of the following rows shows the count for each letter (both
; upper and lower case) in hexadecimal format.

PRINT_HIST

; table of register use in this part of the code
;    R0 holds the value to print using to monitor (ASCII or string address)
;    R1 holds a pointer to the histogram (x3F00) initially, and is incrimented
;          to point to each bin
;    R2 holds a bin counter (bins remaining to be printed)
;    R3 holds the value to print from the histogram
;    R4 holds a digit counter (hex digits remaining for current value)
;    R5 holds a bit counter (bits remaining for current hex digit)
;    R6 is a temporary register
;    R7

    LD R1,HIST_ADDR     ; load the start address of the histogram
    LD R2,NUM_BINS      ; set the bin counter

PRINT_BIN
    LD R6,HIST_ADDR     ; To get the ASCII for the bin label, we will calculate a difference
    NOT R6,R6           ; between the current bin address and the histogram start address
    ADD R6,R6,#1        ; by subracting the start address from the current address.
    ADD R6,R6,R1        ; R6 now holds the number of the current bin (0...26)

    LD R0,ASCII_AT      ; To get the correct character, add the bin number to '@'
    ADD R0,R0,R6
    OUT                 ; print bin label and space
    LD R0,ASCII_SPACE
    OUT

    ; get the digit to print from memory
    LDR R3,R1,#0

    ; initialize counter registers
    AND R4,R4,#0
    ADD R4,R4,#4

; loop to print single hex digit starts here
NEXT_DIGIT
    AND R0,R0,#0        ; Clear the print register
    ADD R5,R0,#4        ; Use the (cleared) R0 to set bit counter to 4

; loop to read individual bits starts here
NEXT_BIT
    ADD R0,R0,R0
    ADD R3,R3,#0
    BRzp SHIFT
    ADD R0,R0,#1
SHIFT
    ADD R3,R3,R3
    ADD R5,R5,#-1
    BRp NEXT_BIT

    ; here we print one of the four hex characters by converting the hex value to an
    ; ASCII value.  'A' > '0', so all characters will need to be shifted in value by '0',
    ; so we just have to check for an additional shift if it is a number.  xA is the tenth
    ; hex digit, so we have to do N-10, then shift by 'A'.  'A' is 17 more than '0', so we
    ; can just add 7 to the '0' offset we are already doing
PRINT
    ADD R6,R0,#-9
    BRnz ADD_ZERO       ; Skip to add '0' if the digit is a number
    ADD R0,R0,#7        ; Add 7 if the hex digit is a letter
ADD_ZERO
    LD R6,ASCII_ZERO    ; Load x0030 into R6 and add it to the digit
    ADD R0,R0,R6
    OUT

    ; we're done with this digit, so decrement the counter and loop
    ADD R4,R4,#-1
    BRp NEXT_DIGIT

    ; we're done with this bin, print a newline and loop
    LD R0,ASCII_NL
    OUT
    ADD R1,R1,#1        ; increment the histogram pointer to the next bin
    ADD R2,R2,#-1       ; decrement the counter so we know when to stop
    BRp PRINT_BIN

    ADD R2,R2,#-1       ; decrement the bin counter and loop back to the next bin
    BRp PRINT_BIN       ; or skip to DONE if we've printed all the bins


    HALT                ; done


; the data needed by the program
NUM_BINS    .FILL #27      ; 27 loop iterations
NEG_AT      .FILL xFFC0    ; the additive inverse of ASCII '@'
AT_MIN_Z    .FILL xFFE6    ; the difference between ASCII '@' and 'Z'
AT_MIN_BQ   .FILL xFFE0    ; the difference between ASCII '@' and '`'
HIST_ADDR   .FILL x3F00    ; histogram starting address
STR_START   .FILL x4000    ; string starting address

ASCII_NL    .FILL x000A    ; ASCII newline character
ASCII_SPACE .FILL x0020    ; ASCII space character
ASCII_ZERO  .FILL x0030    ; ASCII '0'
ASCII_AT    .FILL x0040    ; ASCII '@'

; for testing, you can use the lines below to include the string in this
; program...
; STR_START    .FILL STRING    ; string starting address
; STRING       .STRINGZ "This is a test of the counting frequency code.  AbCd...WxYz."



    ; the directive below tells the assembler that the program is done
    ; (so do not write any code below it!)

    .END
