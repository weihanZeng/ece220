; ECE 220 MP2 - Eric Roch
; This program impliments a Reverse Polish Notation calculator
; using the LC-3 stack.  Input is parsed in real time and operands
; are pushed to the stack while operators cause the last two values
; in the stack to be poped and the result pushed back to the stack.
; The program will return an error by printing "Invalid Expression"
; if an operator is not able to pop 2 values from the stack, if the
; input is not an integer or one of '+ - * / ^ =', or if there is more
; than one value remaining in the stack after the final operation.
; The result of the calculation is stored in R5, as well as printed
; to the screen in hexadecimal (less the leading 'x').
;
.ORIG x3000

GET_CHAR
    GETC
    OUT

    ; if the input is a space, we can ignore it... get the next one
    LD R6, NEG_SPACE
    ADD R6, R6, R0
    BRz GET_CHAR

    ; if the input is an equals sign, we are done... print the value/error
    LD R6, NEG_EQUAL
    ADD R6, R6, R0
    BRz DONE

    ; otherwise, evaluate to see if it is an operator or operand
    JSR EVALUATE
    BR GET_CHAR

DONE
    LD R6, STACK_TOP
    LD R7, STACK_START
    NOT R6, R6              ; invert STACK_TOP without adding 1 to get -(STACK_TOP + 1)
    ADD R6, R6, R7          ; compare that to STACK_START to see if the stack is empty
    BRnp BAD_INPUT          ; if the adjusted STACK_TOP is equal to the STACK_START
                            ; then there was one item left, which is the answer
    JSR POP
    ADD R5, R0, #0
    JSR PRINT_HEX           ; print hex value in R3
    HALT

BAD_INPUT
    LEA R0, INVALID_STR
    PUTS
    HALT

NEG_SPACE       .FILL xFFE0     ; inverse of ' ' (x0020)
NEG_EQUAL       .FILL xFFC3     ; inverse of '=' (x003D)
INVALID_STR     .STRINGZ "Invalid Expression"


;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;R5- value to print in hexadecimal
PRINT_HEX
    ST R3, PRINT_SaveR3     ; save R3 (used as bit counter)
    ST R4, PRINT_SaveR4     ; save R4 (used as digit counter)
    ST R5, PRINT_SaveR5     ; save R5 (modified to read bits)
    ST R6, PRINT_SaveR6     ; save R6 (used as temporary register)
    ST R7, PRINT_SaveR7     ; save R7 (modified by OUT trap)

    AND R4, R4, #0          ; initialize the digit counter to 4
    ADD R4, R4, #4

HEX_DIGIT
    AND R0, R0, #0          ; clear out R0 and set the bit counter to 4
    ADD R3, R0, #4

HEX_BIT
    ADD R0, R0, R0          ; shift R0 left
    ADD R5, R5, #0          ; setCC on R5
    BRzp HEX_SHIFT          ; if R5 is not negative, skip to shift
    ADD R0, R0, #1          ; otherwise add 1 to R0
HEX_SHIFT
    ADD R5, R5, R5          ; shift R5 left
    ADD R3, R3, #-1         ; decrement the bit counter
    BRp HEX_BIT             ; if we still have bits left, loop

    ADD R6, R0, #-9         ; check if current digit is a letter
    BRnz ADD_ZERO           ; if not, skip to adding '0'
    ADD R0, R0, #7          ; otherwise add 'A'-'0'-10 = 7
ADD_ZERO
    LD R6, ASCII_ZERO       ; add '0' to R0 via R6
    ADD R0, R0, R6
    OUT

    ADD R4, R4, #-1         ; decrement the digit counter
    BRp HEX_DIGIT           ; if we still have digits to print, loop

    LD R3, PRINT_SaveR3     ; restore R3
    LD R4, PRINT_SaveR4     ; restore R4
    LD R5, PRINT_SaveR5     ; restore R5
    LD R6, PRINT_SaveR6     ; restore R6
    LD R7, PRINT_SaveR7     ; restore R7
    RET

PRINT_SaveR3    .BLKW #1
PRINT_SaveR4    .BLKW #1
PRINT_SaveR5    .BLKW #1
PRINT_SaveR6    .BLKW #1
PRINT_SaveR7    .BLKW #1
ASCII_ZERO      .FILL x0030


;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;R0 - character input from keyboard
;R5 - current numerical output
;
;
EVALUATE
    ST R6, EVAL_SaveR6
    ST R7, EVAL_SaveR7

CHECK_NUMERIC
    LD R6, NEG_ZERO         ; compare input with '0'
    ADD R6, R6, R0
    BRn NaN                 ; if R0 < '0', it is not numeric
    ADD R6, R6, #-9
    BRp NaN                 ; if R0 > '0' + 9, it is not numeric

NUMERIC
    ; If the program did not branch to NaN, then the input is numeric,
    ; so subtract '0' to get the decimal value and push to the stack
    LD R6, NEG_ZERO
    ADD R0, R0, R6          ; translate ASCII digit to integer
    JSR PUSH                ; push the operand to the stack
    BR EVAL_RETURN          ; return to get the next input


NaN
    ; If the input is not a number, compare it to each of the operators
    ; if it matches any, jump to the operation section,
    ; otherwise the input is invalid
    LD R6, NEG_TIMES
    ADD R6, R6, R0
    BRz OPERATOR

    LD R6, NEG_PLUS         ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
    ADD R6, R6, R0          ;   Valid Inputs:
    BRz OPERATOR            ;       *       x2A     42
                            ;       +       x2B     43
    LD R6, NEG_MINUS        ;       -       x2D     45
    ADD R6, R6, R0          ;       /       x2F     47
    BRz OPERATOR            ;       0       x30     48
                            ;       ...
    LD R6, NEG_SLASH        ;       9       x39     57
    ADD R6, R6, R0          ;       ^       x5E     94
    BRz OPERATOR            ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;

    LD R6, NEG_EXP
    ADD R6, R6, R0
    BRz OPERATOR

    ; if we haven't branched yet, the input is not a number
    ; and is not an operator, so it must be invalid
    BR BAD_INPUT


OPERATOR
    ; this is basically a switch-case statement.  All operators must
    ; pop two values, so that happens first.  Then the current operator
    ; is compared again to each valid operator and the execution branches
    ; to the appropriate 'case' which applies the operation and pushes
    ; the result.  The result is then moved to R5 and EVALUATE returns.


    ; the popped values are stored in R4 and R3, respectively
    ; because the stack is a LIFO buffer, so the second operand
    ; is the first one to be popped, and should be stored second.
    ADD R6, R0, #0      ; save R0 to R6

    JSR POP             ; pop the first value
    ADD R4, R0, #0      ; ... and store it in R4
    JSR POP             ; pop the second value
    ADD R3, R0, #0      ; ... and store it in R3
    ADD R5, R5, #0      ; check for underflow and branch
    BRnp BAD_INPUT      ; otherwise, carry on

    ADD R0, R6, #0      ; restore R0 from R6

    ; this series of comparisons will apply the correct operator
    LD R6, NEG_TIMES
    ADD R6, R6, R0
    BRz OP_MULT

    LD R6, NEG_PLUS
    ADD R6, R6, R0
    BRz OP_ADD

    LD R6, NEG_MINUS
    ADD R6, R6, R0
    BRz OP_SUBTR

    LD R6, NEG_SLASH
    ADD R6, R6, R0
    BRz OP_DIV

    LD R6, NEG_EXP
    ADD R6, R6, R0
    BRz OP_POW

OP_MULT
    JSR MUL             ; R0 <- R3 * R4
    BR STORE_RESULT

OP_ADD
    JSR PLUS            ; R0 <- R3 + R4
    BR STORE_RESULT

OP_SUBTR
    JSR MIN             ; R0 <- R3 - R4
    BR STORE_RESULT

OP_DIV
    JSR DIV             ; R0 <- R3 / R4
    BR STORE_RESULT

OP_POW
    JSR EXP             ; R0 <- R3 ^ R4
    BR STORE_RESULT

; -- END of OPERATOR section -- ;

STORE_RESULT
    JSR PUSH            ; Stack <- R0
    ADD R5, R0, #0      ; R5 <- R0
EVAL_RETURN
    LD R6, EVAL_SaveR6
    LD R7, EVAL_SaveR7
    RET

EVAL_SaveR6     .BLKW #1
EVAL_SaveR7     .BLKW #1

NEG_TIMES       .FILL xFFD6 ; additive inverse of '*' (x2A)
NEG_PLUS        .FILL xFFD5 ; additive inverse of '+' (x2B)
NEG_MINUS       .FILL xFFD3 ; additive inverse of '-' (x2D)
NEG_SLASH       .FILL xFFD1 ; additive inverse of '/' (x2F)
NEG_ZERO        .FILL xFFD0 ; additive inverse of '0' (x30)
NEG_EXP         .FILL xFFA2 ; additive inverse of '^' (x5E)


;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;input R3, R4
;out R0
PLUS
    ADD R0, R3, R4          ; add R3 and R4
    RET

;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;input R3, R4
;out R0
MIN
    NOT R0, R4              ; negate R4 using R0 to store the result
    ADD R0, R0, #1
    ADD R0, R0, R3          ; R0 <- (-R4) + R3
    RET

;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;input R3, R4
;out R0
MUL
    ST R4, MUL_SaveR4
    AND R0, R0, #0
MUL_LOOP
    ADD R0, R0, R3
    ADD R4, R4, #-1
    BRp MUL_LOOP
    LD R4, MUL_SaveR4
    RET

MUL_SaveR4      .BLKW #1

;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;input R3, R4
;out R0
DIV
    ST R3, DIV_SaveR3
    AND R0, R0, #0          ; set R0 to -1
    ADD R0, R0, #-1
    NOT R4, R4              ; negate R4
    ADD R4, R4, #1
DIV_LOOP
    ADD R0, R0, #1          ; increment subtraction count
    ADD R3, R3, R4          ; try another subtraction
    BRzp DIV_LOOP           ; if R3 is still > 0, subtract again

    LD R3, DIV_SaveR3       ; restore R3
    NOT R4, R4              ; restore R4 by negating again
    ADD R4, R4, #1
    RET

DIV_SaveR3      .BLKW #1

;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;input R3, R4
;out R0
EXP
    ST R3, EXP_SaveR3       ; save R3 (used to store intermediate result)
    ST R4, EXP_SaveR4       ; save R4 (so we can restore the power)
    ST R5, EXP_SaveR5       ; save R5 (used to store power counter)
    ST R7, EXP_SaveR7       ; save R7 (so we can return)

    ADD R5, R4, #0          ; move the exponent to R5
    ADD R4, R3, #0          ; move the base to R4
    AND R3, R3, #0          ; set R3 to 1
    ADD R3, R3, #1
EXP_LOOP
    ADD R5, R5, #-1         ; decrement the power, if negative then we are done
    BRn EXP_DONE            ; else, multiply current total by the base
    JSR MUL                 ; R0 <- R3 * R4
    ADD R3, R0, #0          ; put the result back in R3
    BR EXP_LOOP

EXP_DONE
    ADD R0, R3, #0          ; put the final result in R0

    LD R3, EXP_SaveR3
    LD R4, EXP_SaveR4
    LD R5, EXP_SaveR5
    LD R7, EXP_SaveR7
    RET

EXP_SaveR3      .BLKW #1
EXP_SaveR4      .BLKW #1
EXP_SaveR5      .BLKW #1
EXP_SaveR7      .BLKW #1

;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;IN:R0, OUT:R5 (0-success, 1-fail/overflow)
;R3: STACK_END R4: STACK_TOP
;
PUSH
    ST R3, PUSH_SaveR3      ; save R3
    ST R4, PUSH_SaveR4      ; save R4
    AND R5, R5, #0          ;
    LD R3, STACK_END        ;
    LD R4, STACk_TOP        ;
    ADD R3, R3, #-1         ;
    NOT R3, R3              ;
    ADD R3, R3, #1          ;
    ADD R3, R3, R4          ;
    BRz OVERFLOW            ; stack is full
    STR R0, R4, #0          ; no overflow, store value in the stack
    ADD R4, R4, #-1         ; move top of the stack
    ST R4, STACK_TOP        ; store top of stack pointer
    BRnzp DONE_PUSH         ;
OVERFLOW
    ADD R5, R5, #1          ;
DONE_PUSH
    LD R3, PUSH_SaveR3      ;
    LD R4, PUSH_SaveR4      ;
    RET

PUSH_SaveR3    .BLKW #1     ;
PUSH_SaveR4    .BLKW #1     ;

;;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;OUT: R0, OUT R5 (0-success, 1-fail/underflow)
;R3 STACK_START R4 STACK_TOP
;
POP
    ST R3, POP_SaveR3       ; save R3
    ST R4, POP_SaveR4       ; save R3
    AND R5, R5, #0          ; clear R5
    LD R3, STACK_START      ;
    LD R4, STACK_TOP        ;
    NOT R3, R3              ;
    ADD R3, R3, #1          ;
    ADD R3, R3, R4          ;
    BRz UNDERFLOW           ;
    ADD R4, R4, #1          ;
    LDR R0, R4, #0          ;
    ST R4, STACK_TOP        ;
    BRnzp DONE_POP          ;
UNDERFLOW
    ADD R5, R5, #1          ;
DONE_POP
    LD R3, POP_SaveR3       ;
    LD R4, POP_SaveR4       ;
    RET

POP_SaveR3      .BLKW #1        ;
POP_SaveR4      .BLKW #1        ;

STACK_END       .FILL x3FF0     ;
STACK_START     .FILL x4000     ;
STACK_TOP       .FILL x4000     ;


.END
