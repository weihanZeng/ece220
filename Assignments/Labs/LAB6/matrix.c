/* Function matrix_multiply
 * matrixA, matrixB, and matrix C are matrices represented as
 * one-dimensional arrays in row-major order. This function must
 * preform matrix multiplication so that C=A*B.
 * INPUT: matrixA, a one dimensional array of size m*k
 *        matrixB, a one dimensional double array of size k*n
 * OUTPUT: matrixC, a one dimensional double array of size m*n
 */
void matrix_multiply(double *matrixA,double *matrixB,double *matrixC,int m,int k,int n)
{
    for (int y = 0; y < m; y ++) {
        for (int x = 0; x < n; x ++) {
            double val = 0;
            // this block will run once for every index in C
            for (int l = 0; l < k; l ++) {
                double A = matrixA[l + y*k];
                double B = matrixB[x + l*k];
                val += A * B;
            }
            matrixC[x + y*n] = val;
        }
    }
}
