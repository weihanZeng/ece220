#include "sliding.h"
/*  Slide all values of array up
*/

int xy2index(int x, int y, int width) {
    return x + y*width;
}

void slide_up(int* my_array, int rows, int cols){
    int x,y;
    for (x = 0; x < cols; x ++) {
        for (y = 0; y < rows; y ++) {
            if (my_array[xy2index(x,y,cols)] != -1) {
                int target_row = y;
                int i;
                // find the first empty cell in the column and break
                for (i = 0; i < y; i ++) {
                    if (my_array[xy2index(x,i,cols)] == -1) {
                        target_row = i;
                        break;
                    }
                }
                // if target == y, we can't shift
                if (target_row != y) {
                    my_array[xy2index(x,target_row,cols)] = my_array[xy2index(x,y,cols)];
                    my_array[xy2index(x,y,cols)] = -1;
                }

            }
        }
    }

    return;
}
