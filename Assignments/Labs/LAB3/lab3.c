#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.1415926

void getInput(int *n, int *w1, int *w2);
void printOutput(int n, int w1, int w2);
float f(float x, int w1, int w2);

int main(){
    // Declare variables
    int n, omega1, omega2;
    // Get user input
    getInput(&n, &omega1, &omega2);
    // print the output table
    printOutput(n, omega1, omega2);

    return 0;
}

void getInput(int *n, int *w1, int *w2) {
    printf("Enter the values for n, omega1 and omega2\n");
    scanf("%d %d %d", n, w1, w2);
}

void printOutput(int n, int w1, int w2) {
    for (int i = 0; i < n; i ++) {
        float x = i * PI / n;
        float y = f(x, w1, w2);
        printf("(%f, %f)\n", x, y);
    }
}

float f(float x, int w1, int w2) {
    return sin(w1 * x) + (sin(w2 * x))/2;
}
