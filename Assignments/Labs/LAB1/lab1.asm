;
; R0 - ASCII value to display on monitor
; R1 - ASCII '0'
; R2
; R3 - value to print in hexadecimal
; R4 - digit counter (number of hex values remaining)
; R5 - bit counter (number of bits remaining for current digit)
; R6 - digit (current hex value to print)
; R7

.ORIG x3000
            LD    R1,ASCII_ZERO

            ;start by setting the digit counter to 4
            ;and marking the beginning of the digit loop
            AND   R4,R4,#0
            ADD   R4,R4,#4
NEXT_DIGIT  BRnz  END

            AND   R6,R6,#0  ;set digit to 0
            ADD   R5,R6,#4  ;set bit counter to digit + 4 (=4)

NEXT_BIT    BRnz  PRINT
            ADD   R6,R6,R6  ;shift digit left
            ADD   R3,R3,#0  ;set CC
            BRzp  SHIFT     ;if R3 > 0, skip adding 1
            ADD   R6,R6,#1
SHIFT       ADD   R3,R3,R3  ;shift R3 left
            ADD   R5,R5,#-1 ;decrement bit counter
            BR    NEXT_BIT

            ;to convert a hex value to the ASCII value for the hex digit, we have to add
            ;an offset. '0' is x30, so all hex digits will at least be shifted by x30.
            ;If the hex digit is a letter, it needs to start at x41 but also needs to be shifted
            ;back by 10 so that xA looks like x0 when we add 'A'. This is the same as adding 'A'-10,
            ;or x37. Since we already have to add x30 to everything, we just add 7 more to letters.
PRINT       ADD   R0,R6,#0  ;put the digit in R0
            ADD   R6,R6,#-9 ;subtract 9 to see if we need to add 'A'-10 or '0'

            BRnz  ADD_ZERO
            ADD   R0,R0,#7  ;only add 7 if digit is a letter (>9)
ADD_ZERO    ADD   R0,R0,R1

            OUT
            ADD   R4,R4,#-1
            BR    NEXT_DIGIT

END         HALT

ASCII_ZERO  .FILL x0030

.END
