int isSafe(int board[N][N], int row, int col) {
    // check the same column in previous rows
    for (int i = 0; i < row; i ++) {
        if (board[i][col] == 1) {
            return 0;
        }
    }

    // starting one up and one left, check along the main diagonal
    for (int i = row-1, j = col-1; i >= 0 && j >= 0; i --, j--) {
        if (board[i][j] == 1) {
            return 0;
        }
    }

    // starting one up and one right, check the anti-diagonal
    for (int i = row-1, j = col+1; i >= 0 && j < N; i --, j++) {
        if (board[i][j] == 1) {
            return 0;
        }
    }

    for (int i = 0; i < row; i ++) {
        for (int j = 0; j < N; j ++) {
            if (board[i][j] == 1 &&
                ((j == col) || (i-j == row-col) || (i+j) == row+col)) {
                    return 0;
            }
        }
    }

    // if nothing returned 0, then it must be safe
    return 1;
}
