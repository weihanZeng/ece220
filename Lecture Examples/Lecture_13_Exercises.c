#include <stdio.h>

#define ROW 3
#define COL 4
#define SIZE 5

void row_interchange(int matrix[5][5], int x, int y);
void transpose(int in_matrix[ROW][COL], int out_matrix[COL][ROW]);
void transpose2(int *in_matrix, int *out_matrix);
void bubble_sort(int array[]);
void insertion_sort(int array[]);

int main() {
    // int matrix[5][5] = {
    //     { 1,  2,  3,  4,  5},
    //     { 6,  7,  8,  9, 10},
    //     {11, 12, 13, 14, 15},
    //     {16, 17, 18, 19, 20},
    //     {21, 22, 23, 24, 25}};
    //
    // row_interchange(matrix, 2, 4);
    // int i, j;
    // for (i = 0; i < 5; i ++) {
    //     for (j = 0; j < 5; j ++) {
    //         printf("%d ", matrix[i][j]);
    //     }
    //     printf("\n");
    // }

    // int in_matrix[ROW][COL] = {
    //     {1,  2,  3,  4},
    //     {5,  6,  7,  8},
    //     {9, 10, 11, 12}};
    // int out_matrix[COL][ROW];
    //
    // // transpose(in_matrix, out_matrix);
    // transpose2(*in_matrix, *out_matrix);
    // int i,j;
    // for (i = 0; i < COL; i ++) {
    //     for (j = 0; j < ROW; j ++) {
    //         printf("%d ", out_matrix[i][j]);
    //     }
    //     printf("\n");
    // }

    int array[SIZE] = {3,4,2,1,5};
    // bubble_sort(array);
    insertion_sort(array);

    int i;
    for (i = 0; i < SIZE; i ++) {
        printf("%d ", array[i]);
    }

    return 0;
}

void
row_interchange(int matrix[5][5], int x, int y)
{
    int i;
    for (i = 0; i < 5; i ++) {
        matrix[x][i] ^= matrix[y][i];
        matrix[y][i] ^= matrix[x][i];
        matrix[x][i] ^= matrix[y][i];
    }
}


void
transpose(int in_matrix[ROW][COL], int out_matrix[COL][ROW])
{
    int i,j;
    for (i = 0; i < ROW; i ++) {
        for (j = 0; j < COL; j ++) {
            out_matrix[j][i] = in_matrix[i][j];
        }
    }
}

void
transpose2(int *in_matrix, int *out_matrix)
{
    int i,j;
    for (i = 0; i < ROW; i ++) {
        for (j = 0; j < COL; j ++) {
            out_matrix[j*ROW + i] = in_matrix[i*COL + j];
        }
    }
}


void
bubble_sort(int array[])
{
    int last_index = SIZE;  // index of last unsorted item
    int did_swap;
    do {
        did_swap = 0;
        int i;
        for (i = 0; i < last_index - 1; i ++) {
            // do an XOR swap if first value is less than the next
            if (array[i] > array[i+1]) {
                array[i] ^= array[i+1];
                array[i+1] ^= array[i];
                array[i] ^= array[i+1];

                did_swap = 1;
            }
        }
    } while (did_swap);
}

void
insertion_sort(int array[])
{
    int first_index = 1;     // index of first unsorted item
    while (first_index < SIZE) {
        int k;
        for (k = 0; k < SIZE; k ++) {
            printf("%d ", array[k]);
        }
        printf("\n");

        int x = array[first_index];
        int i;
        for (i = first_index - 1; i >= 0; i --) {
            if (array[i] > x) {
                array[i+1] = array[i];
            }
            else {
                array[i] = x;
                break;
            }
        }
        first_index ++;
    }
}
