.ORIG x3000

;your program starts here

    LD R6, INPUT_ADDR       ; pointer to user input
    LD R5, NEG_NL           ; use to compare input for newline
    LD R4, NEG_CR
GET_INPUT
    JSR MY_GETC             ; get a character from the terminal
    JSR MY_OUT              ; and echo it to the screen
    ADD R1, R0, R5          ; compare it to NL, and finish if it is
    BRz CHECK
    ADD R1, R0, R4
    BRz CHECK
    STR R0, R6, #0          ; otherwise store input at data pointer
    ADD R6, R6, #1          ; and incriment pointer
    BR GET_INPUT            ; loop back for the next input

CHECK
    LD R1, INPUT_ADDR       ; store the negative of the input address
    NOT R1, R1              ; in R1 so we can compare the current pointer
    ADD R1, R1, #1          ; with the start of the user data

    ADD R2, R1, R6          ; R2 <- POINTER - START
    ADD R2, R2, #-10        ; If the pointer was not exactly 10 addresses
    BRnp INVALID            ; away from the start, there were not 10 characters

    AND R4, R4, #0          ; init counter to check 10 inputs
    ADD R4, R4, #9
    LD R3, INPUT_ADDR       ; get the start address
    LD R1, NEG_ZERO
LOOP
    ADD R5, R3, R4          ; get the address of the digit to check
    LDR R0, R5, #0          ; get the digit from memory
    ADD R0, R0, R1
    BRn INVALID             ; R0 < '0', invalid
    ADD R0, R0, #9
    BRp INVALID             ; R0 > '9', invalid
    ADD R4, R4, #-1
    BRzp LOOP
    BR VALID

INVALID
    LEA R0, INV_MSG
    PUTS
    HALT

VALID
    LEA R0, VAL_MSG
    PUTS
    HALT

NEG_ZERO    .FILL xFFDF
NEG_NL      .FILL xFFF5
NEG_CR      .FILL xFFF3
INPUT_ADDR  .FILL x5000

; IN:
; OUT: R0 - ASCII value from keyboard
MY_GETC
    LDI R0, KBSR        ; Check the KB status
    BRzp MY_GETC        ; if not set, loop again
    LDI R0, KBDR        ; load KB data into R0
    RET


; IN: R0 - ASCII value to display
; OUT:
MY_OUT
    ST R1, OUT_SaveR1   ; save R1
WAIT_4_DISP
    LDI R1, DSR         ; Check the DISP status
    BRzp WAIT_4_DISP    ; if not set, loop again
    STI R0, DDR         ; store R0 in DDR
    LD R1, OUT_SaveR1   ; restore R1
    RET

OUT_SaveR1 .BLKW #1


KBSR    .FILL xFE00
KBDR    .FILL xFE02
DSR     .FILL xFE04
DDR     .FILL xFE06
INV_MSG .STRINGZ "Invalid Phone Number."
VAL_MSG .STRINGZ "Valid Phone Number."

.END
