
.ORIG x3000

INIT_REGS
    AND R0, R0, #0      ; count
    AND R1, R1, #0
    AND R2, R2, #0
    AND R3, R3, #0
    AND R4, R4, #0
    AND R5, R5, #0      ; n-1
    AND R6, R6, #0      ; n

    LD R6, INPUT_ADDRESS
    LDR R6, R6, #0

; TODO
LOOP
    ADD R6, R6, #0      ; setCC on R6 (n)
    BRz DONE
    ADD R5, R6, #-1     ; store (n-1) in R5
    AND R6, R5, R6      ; n = n & (n-1)
    ADD R0, R0, #1      ; increment count
    BR LOOP

DONE
; END TODO

	STI R0, OUTPUT_VALUE

HALT

; Memory definition.
INPUT_ADDRESS   .FILL x5000
OUTPUT_VALUE    .FILL x5000

.END
