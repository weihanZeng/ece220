.ORIG x3000

    LD R1, INPUT
    JSR FIBON
    ST R6, RESULT
    HALT

;your code goes here
FIBON

    ADD R0, R1, #-2     ; check if k <= 2
    BRnz K_2
    AND R3, R3, #0      ; k >= 3, so set F_low (R3) = 1
    AND R4, R4, #0      ; and F_high (R4) = 1
    ADD R3, R3, #1
    ADD R4, R4, #1
    ADD R1, R1, #-3     ; looping from 3..k is the same as doing
                        ; 0..(k-3), except we can compare with zero
FIB_LOOP
    ADD R5, R3, R4      ; F_sum (R5) = F_low + F_high
    ADD R3, R4, #0      ; F_low = F_high
    ADD R4, R5, #0      ; F_high = F_sum
    ADD R1, R1, #-1     ; decrement k
    BRzp FIB_LOOP
    ADD R6, R4, #0      ; set result
    RET

K_2
    AND R6, R6, #0
    ADD R6, R6, #1
    RET

INPUT 	.FILL x0007
RESULT	.BLKW #1
.END
